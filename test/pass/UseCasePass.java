package pass;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

import Game.GameManager;

/**
 * 
 * @author Ali
 *
 * Testing the passed used Case for game. The winning of both players and ties should be equal to the count of game.
 */
public class UseCasePass {
	
	private GameManager 	gm				=	null;
	private int				gamecount		= 10;

	@Before
	public void prepareTest(){
		gm = GameManager.getGameManagerInstance("Fido", "Dido", gamecount);
	}
	
	@Test
	public void checkGamePlay(){
		gm.showLog = false;
		gm.PlayGame();
		assertEquals(gamecount, (gm.playerA.win_count + gm.playerB.win_count + gm.ties));
	}
	
}
