package fail;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import Game.GameManager;

/**
 * 
 * @author Ali
 * 
 */
/**
 * The test package is divided in the tests that should be failed or passed. This is functional testing,
 * checking the null initiation of classes.
 *
 */
public class UseCaseFail {
	private GameManager gm		 = null;
	
	
	@Test(expected=NullPointerException.class)
	  public void testNullExceptionGameManager() {
		 gm.PlayGame();
		 fail("Fails due to null exception.");
	  }
	
	
	
}
