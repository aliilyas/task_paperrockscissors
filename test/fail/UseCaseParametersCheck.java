package fail;

import static org.junit.Assert.fail;

import org.junit.Before;
import org.junit.Test;

import Game.GameManager;

public class UseCaseParametersCheck {
	
	/**
	 * Checking the null and negative parameter for instantiating Game Manager.
	 */
		private GameManager gm		 = null;
		
		@Before
		public void prepareTest(){
			gm = GameManager.getGameManagerInstance(null, null, -1);
		}
		@Test(expected=NullPointerException.class)
		  public void testNullExceptionGameManager() {
			 gm.PlayGame();
		  }
		
		

}
