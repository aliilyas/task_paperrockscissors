package players;
/**
 * 
 * @author Ali
 * @since 2015-05-29
 */
public class PlayerA  implements IPlayers{

	/**
	 * It's okay to break conventions every now and then, keeping variables public.
	 */
	public int win_count		=	0;
	public String player_name	=	"PlayerA";
	
	public  PlayerA(String aName) {
		if(aName!=null)
			this.player_name = aName;
	}
	
	public Strategies chooseStrategy(){
		return IPlayers.Strategies.Paper;
	}
	
	
	

}
