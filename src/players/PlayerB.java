package players;
/**
 * 
 * @author Ali
 * @since 2015-05-29
 *
 */
public class PlayerB implements IPlayers {

	/**
	 * It's okay to break conventions every now and then, keeping variables public.
	 */
	public int win_count		=	0;
	public String player_name	=	"PlayerB";

	public  PlayerB(String aName) {
		if(aName !=null)
			this.player_name = aName;
	}

	public Strategies chooseStrategy(){
	    return Strategies.values()[(int) (Math.random() * Strategies.values().length)];
		
	}
	
	
	
}
