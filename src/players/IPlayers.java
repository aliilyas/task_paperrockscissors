package players;

/**
 * @author Ali
 * @since 2015-05-29
 */

/* Basic Interface for the players , it contains the Strategies and a function that returns the strategy*/
public interface IPlayers {
	
	public enum Strategies {
		Rock("Rock"),
		Scissors("Scissors"),
		Paper("Paper");
		
		 private final String strategy;       

	    private Strategies(String s) {
	    	strategy = s;
	    }
	    public String toString(){
	        return strategy;
	    }
	};
	
	public Strategies chooseStrategy();
	

}
