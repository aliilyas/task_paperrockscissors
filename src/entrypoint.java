import Game.GameManager;


public class entrypoint {

	public static void main(String[] args) {
		
		
		GameManager gm = GameManager.getGameManagerInstance("SuperMan", "SuperGirl", 100);
		gm.showLog = false;
		gm.PlayGame();
		
		System.out.println("Player A :" + gm.playerA.player_name +" wins " + gm.playerA.win_count + " of " + gm.game_count + " games");
		System.out.println("Player B :" + gm.playerB.player_name +" wins " + gm.playerB.win_count + " of " + gm.game_count + " games");
		System.out.println("Tie :" + gm.ties + " of " + gm.game_count + " games");

	}

}
