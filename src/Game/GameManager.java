package Game;

import static org.junit.Assert.*;

import players.IPlayers.Strategies;
import players.PlayerA;
import players.PlayerB;

/**
 * 
 * @author Ali
 * @since 2015-05-29
 */
public class GameManager {

	/**
	 * Instead of logger, I am using simple print.
	 */
	public boolean showLog					=	false;

	private static GameManager gm_instance	=	null;

	/**
	 * It's okay to break conventions every now and then, keeping variables public.
	 */
	public PlayerA playerA 					=	null;
	public PlayerB playerB					= 	null;

	public int game_count					=	0;
	public int failed_games					=	0;
	public int ties							=	0;

	public static GameManager getGameManagerInstance(String aPlayerName ,String bPlayerName, int gameCount){
		if(gm_instance == null)
			gm_instance = new GameManager(aPlayerName,bPlayerName,gameCount);

		return gm_instance;
	}

	/**
	 * Private Constructor so the instantiation can not be made outside of the class,
	 * and to avoid generalization.
	 */
	private GameManager(String aPlayerName, String bPlayerName, int gameCount){
		this.game_count = (gameCount < 0 ) ? 100 : gameCount;
		
		assertTrue("The game count should be greater than 0", gameCount > 0);
		
		initPlayers(aPlayerName , bPlayerName);
	}

	private void initPlayers(String aPlayerName , String bPlayerName){
		playerA = new PlayerA(aPlayerName);
		playerB = new PlayerB(bPlayerName);

	}

	public void PlayGame(){
		for (int i = 1 ; i < this.game_count + 1 ; i++) {
			Strategies PlayerAStrategy = this.playerA.chooseStrategy();
			Strategies playersBStrategy = this.playerB.chooseStrategy();
			
		     assertTrue("The player A's strategy should be Paper",PlayerAStrategy.equals(Strategies.Paper) );

			if(showLog){
				System.out.println("Game # " + i);
				System.out.println("Player A strategy : " + PlayerAStrategy.toString() + " ---  Player B strategy: " + playersBStrategy.toString());
			}
			String winningStrategy = winingStrategy(PlayerAStrategy, playersBStrategy);
			if(winningStrategy.equals(PlayerAStrategy.toString())){
				playerA.win_count++;
				if(showLog)
					System.out.println("Player A wins");
			}else if (winningStrategy.equals(playersBStrategy.toString())){
				playerB.win_count++;
				if(showLog)
					System.out.println("Player B wins");
			}else if (winningStrategy.equals("ties")){
				this.ties++;
				if(showLog)
					System.out.println("It's a tie");
			}
			else if (winningStrategy.equals("")){
				failed_games++;
				if(showLog)
					System.out.println("something horrible has happened, save your souls world gonna destruct ");
			}
			if(showLog)
				System.out.println(" ---------------- ");


		}

	}

	/**
	 * The game rules are defined here, for more dynamic version of game manager, the game manager should be
	 * implementation of some interface so we can have multiple winningStrategies. But for the brevity of the task,
	 * I am encapsulating game rules in game manager.
	 * 
	 * @param aPlayerStrategy
	 * @param bPlayersStrategy
	 * @return
	 */

	private String winingStrategy(Strategies aPlayerStrategy , Strategies bPlayersStrategy){
		if (aPlayerStrategy.equals(Strategies.Paper) && bPlayersStrategy.equals(Strategies.Paper))
			return "ties";

		else if (aPlayerStrategy.equals(Strategies.Paper) && bPlayersStrategy.equals(Strategies.Rock))
			return Strategies.Paper.toString();

		else if (aPlayerStrategy.equals(Strategies.Paper) && bPlayersStrategy.equals(Strategies.Scissors))
			return Strategies.Scissors.toString();

		else if (aPlayerStrategy.equals(Strategies.Rock) && bPlayersStrategy.equals(Strategies.Rock))
			return "ties";

		else if (aPlayerStrategy.equals(Strategies.Rock) && bPlayersStrategy.equals(Strategies.Paper))
			return Strategies.Paper.toString();

		else if (aPlayerStrategy.equals(Strategies.Rock) && bPlayersStrategy.equals(Strategies.Scissors))
			return Strategies.Rock.toString();

		else if (aPlayerStrategy.equals(Strategies.Scissors) && bPlayersStrategy.equals(Strategies.Scissors))
			return "ties";

		else if (aPlayerStrategy.equals(Strategies.Scissors) && bPlayersStrategy.equals(Strategies.Paper))
			return Strategies.Scissors.toString();

		else if (aPlayerStrategy.equals(Strategies.Scissors) && bPlayersStrategy.equals(Strategies.Rock))
			return Strategies.Rock.toString();

		else if (bPlayersStrategy.equals(Strategies.Paper) && aPlayerStrategy.equals(Strategies.Rock))
			return Strategies.Paper.toString();

		else if (bPlayersStrategy.equals(Strategies.Paper) && aPlayerStrategy.equals(Strategies.Scissors))
			return Strategies.Scissors.toString();


		else if (bPlayersStrategy.equals(Strategies.Rock) && aPlayerStrategy.equals(Strategies.Paper))
			return Strategies.Paper.toString();

		else if (bPlayersStrategy.equals(Strategies.Rock) && aPlayerStrategy.equals(Strategies.Scissors))
			return Strategies.Rock.toString();

		else if (bPlayersStrategy.equals(Strategies.Scissors) && aPlayerStrategy.equals(Strategies.Paper))
			return Strategies.Scissors.toString();

		else if (bPlayersStrategy.equals(Strategies.Scissors) && aPlayerStrategy.equals(Strategies.Rock))
			return Strategies.Rock.toString();

		return "";


	}


}
